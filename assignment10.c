#include <stdio.h>
#include <stdlib.h>

int main() 
   {
int x;
printf("\nInput an integer: "); 
scanf("%d", &x);
if(x >=0 && x <= 20) 
{
printf("It exists in Range [0, 20]\n");
} 
else 
{
printf("Outside the range\n");
}	
FILE* fPtr;
char ch;
fPtr = fopen("assignment10.c","r");
if(fPtr == NULL)
{
printf("Unable to open file.\n");
printf("Please check whether file exists\n");
exit(EXIT_FAILURE);
}
{
printf("\n\nFile opened successfully \n\n");
do
{
ch = fgetc(fPtr);
putchar(ch);
}
while(ch != EOF);
fclose(fPtr);
return 0;
}}